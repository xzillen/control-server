package de.martin.cs.main;



import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import de.martin.cs.FileManager.FileManager;
import de.martin.cs.Listener.InventoryClickevent;
import de.martin.cs.Listener.Join;
import de.martin.cs.commands.CMD_Control;

public class Main extends JavaPlugin{
	
	File file = new File("plugins//ControlServer//Message.yml");
	FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	File inv = new File("plugins//ControlServer//Inventory.yml");
	FileConfiguration inve = YamlConfiguration.loadConfiguration(file);
	
	File Booleans = new File("plugins//ControlServer//Booleans.yml");
	FileConfiguration boo = YamlConfiguration.loadConfiguration(file);
	
	File perms = new File("plugins//ControlServer//Permissions.yml");
	FileConfiguration perm = YamlConfiguration.loadConfiguration(file);
	
	
	File items = new File("plugins//ControlServer//items.yml");
	FileConfiguration it = YamlConfiguration.loadConfiguration(items);
	
	public FileManager fm;
	
	public void onEnable() {
		fm = new FileManager();
		if(!fm.exist()){
			fm.create();
		}
		checkFileExiting();
		loadListener();
		loadCommands();
	}
	private void loadListener(){
		PluginManager pm = Bukkit.getPluginManager();
		pm.registerEvents(new Join(), this);
		pm.registerEvents(new InventoryClickevent(), this);
		
	}
	private void loadCommands(){
		getCommand("control").setExecutor(new CMD_Control());
	}
	
	public void checkFileExiting() {
		if (file.exists()) {
			Bukkit.getConsoleSender().sendMessage("�aFile Existiert!");
			}else {
				Bukkit.getConsoleSender().sendMessage("�cFile Existiert nicht!");
			}
		if (inv.exists()) {
			Bukkit.getConsoleSender().sendMessage("�aInv Existiert");
			}else {
				Bukkit.getConsoleSender().sendMessage("�cinv Existiert nicht");
			}
		if (Booleans.exists()) {
			Bukkit.getConsoleSender().sendMessage("�aBooleans Existiert");
			}else {
				Bukkit.getConsoleSender().sendMessage("�cBooleans Existiert nicht");
			}
		if (perms.exists()) {
			Bukkit.getConsoleSender().sendMessage("�aPerms Existiert");
			}else {
				Bukkit.getConsoleSender().sendMessage("�cFerms Existiert nicht");
			}
		if (items.exists()) {
			Bukkit.getConsoleSender().sendMessage("�aItems Existiert");
			}else {
				Bukkit.getConsoleSender().sendMessage("�cItems Existiert nicht");
			}
				
	}
}
