package de.martin.cs.Listener;

import java.io.File;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class Join implements Listener {
	
	File ordner = new File("plugins//ControlServer");
	File file = new File("plugins//ControlServer//Message.yml");
	FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	File inv = new File("plugins//ControlServer//Inventory.yml");
	FileConfiguration inve = YamlConfiguration.loadConfiguration(inv);
	
	File Booleans = new File("plugins//ControlServer//Booleans.yml");
	FileConfiguration boo = YamlConfiguration.loadConfiguration(Booleans);
	
	File perms = new File("plugins//ControlServer//Permissions.yml");
	FileConfiguration perm = YamlConfiguration.loadConfiguration(perms);
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		
		if(boo.getBoolean("Settings.Joinmessage.Send") == false){
			e.setJoinMessage(null);
		} else {
			if(boo.getBoolean("Settings.Joinmessage.Send") == true){
				String msg = cfg.getString("Settings.Message.Joinmessage");
				msg = msg.replace("%Player%", p.getName());
				msg = msg.replace("%Prefix%", cfg.getString("Settings.Message.Prefix"));
				
				e.setJoinMessage(ChatColor.translateAlternateColorCodes('&', msg));
			}
		}
	}
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		
		if(boo.getBoolean("Settings.Quitmessage.Send") == false){
			e.setQuitMessage(null);
		} else {
			if(boo.getBoolean("Settings.Quitmessage.Send") == true){
				String msg = cfg.getString("Settings.Message.Quitmessage");
				msg = msg.replace("%Player%", p.getName());
				msg = msg.replace("%Prefix%", cfg.getString("Settings.Message.Prefix"));
				
				e.setQuitMessage(ChatColor.translateAlternateColorCodes('&', msg));
			}
		}
	}
}