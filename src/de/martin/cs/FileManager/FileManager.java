package de.martin.cs.FileManager;



import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public  class FileManager {
	
	File ordner = new File("plugins//ControlServer");
	File file = new File("plugins//ControlServer//Message.yml");
	FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	File inv = new File("plugins//ControlServer//Inventory.yml");
	FileConfiguration inve = YamlConfiguration.loadConfiguration(inv);
	
	File Booleans = new File("plugins//ControlServer//Booleans.yml");
	FileConfiguration boo = YamlConfiguration.loadConfiguration(Booleans);
	
	File perms = new File("plugins//ControlServer//Permissions.yml");
	FileConfiguration perm = YamlConfiguration.loadConfiguration(perms);
	
	File items = new File("plugins//ControlServer//items.yml");
	FileConfiguration it = YamlConfiguration.loadConfiguration(items);
	
	public void create(){
		try {
			ordner.mkdir();
			file.createNewFile();
			inv.createNewFile();
			Booleans.createNewFile();
			perms.createNewFile();
			items.createNewFile();
			//Message
			cfg.set("Settings.Message.Prefix", "&8[&aSettingsControl&8] ");
			cfg.set("Settings.Message.Joinmessage", "%Prefix% &6Der Spieler &a%Player% &6hat den Server &eBetreten!");
			cfg.set("Settings.Message.Quitmessage", "%Prefix% &6Der Spieler &a%Player% &6hat den Server &cVerlassen!");
			cfg.set("Settings.Message.NoPerms", "%Prefix% &cDu hast keine Rechte daf�r!");
			
			//Inventar
			inve.set("Settings.InvName.Name", "SettingsControl");
			//Ints
			inve.set("Settings.InvName.Size", 9);
			
			
			//boolean
			boo.set("Settings.Joinmessage.Send", false);
			boo.set("Settings.Quitmessage.Send", false);
			
			//Perms
			perm.set("Settings.Perms.control", "settings.openinv");
			
			//items
			it.set("Item_1.id", 5);
			it.set("Item_1.subid", 0);
			it.set("Item_1.Amount", 5);
			it.set("Item_1.Name", 5);
			it.set("Item_1.lore", new ArrayList<String>());
			
			cfg.save(file);
			inve.save(inv);
			boo.save(Booleans);
			perm.save(perms);
			it.save(items);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public boolean exist(){
		if(file.exists() || inv.exists() || Booleans.exists() || perms.exists() || items.exists()){
			return true;
		} else {
			return false;
		}
	}
}