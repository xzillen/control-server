package de.martin.cs.commands;



import java.io.File;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;



public class CMD_Control implements CommandExecutor {

	File ordner = new File("plugins//ControlServer");
	File file = new File("plugins//ControlServer//Message.yml");
	FileConfiguration cfg = YamlConfiguration.loadConfiguration(file);
	
	File inv = new File("plugins//ControlServer//Inventory.yml");
	FileConfiguration inve = YamlConfiguration.loadConfiguration(inv);
	
	File Booleans = new File("plugins//ControlServer//Booleans.yml");
	FileConfiguration boo = YamlConfiguration.loadConfiguration(Booleans);
	
	File perms = new File("plugins//ControlServer//Permissions.yml");
	FileConfiguration perm = YamlConfiguration.loadConfiguration(perms);
	
	File items = new File("plugins//ControlServer//items.yml");
	FileConfiguration it = YamlConfiguration.loadConfiguration(items);
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] string) {
		Player p = (Player)sender;
		
		if (cmd.getName().equalsIgnoreCase("control")) {
			if (!(p.hasPermission(perm.getString("Settings.Perms.control")))) {		
				String msg = cfg.getString("Settings.Message.NoPerms");				
				msg = msg.replace("%Prefix%", cfg.getString("Settings.Message.Prefix"));
				
				p.sendMessage(ChatColor.translateAlternateColorCodes('&', msg));
				
				
			}else {
				if (cmd.getName().equalsIgnoreCase("control")) {
					Inventory inv = Bukkit.createInventory(null, inve.getInt("Settings.InvName.Size"), ChatColor.translateAlternateColorCodes('&', inve.getString("Settings.InvName.Name")));
					
					String name = it.getString("Item_1.Name");
					int id = it.getInt("Item_1.id");
					int subid = it.getInt("Item_1.subid");
					int amount = it.getInt("Item_1.Amount");
					List<String> lore = it.getStringList("Item_1.lore");						
					@SuppressWarnings("deprecation")
					ItemStack item = new ItemStack(Material.getMaterial(id), amount, (byte) subid);
							
					inv.setItem(0, CreateItemWithID(Material.IRON_DOOR, 0, 1, "�eSpwan setzten"));
					inv.setItem(8, CreateItemWithID(Material.WATER_BUCKET, 0, 1, "�ekontrolle Server"));
				   p.openInventory(inv);
				}
			}
			
		}
		return true;
	}
	
	public static ItemStack CreateItemWithID(Material mat, int subid, int amount, String Displayname ) {
		ItemStack item = new ItemStack(mat, amount, (short) subid);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName(Displayname);
		item.setItemMeta(meta);
		
		return item;
		
	}

}
